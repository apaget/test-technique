<?php

namespace App\Service;

use App\Entity\Group;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Psr\Log\LoggerInterface;

class ExcelImporter
{
    private $entityManager;
    private $logger;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function import(string $filename): void
    {
        $reader = IOFactory::createReaderForFile($filename);

        try {
            $spreadsheet = $reader->load($filename);
        } catch (\Exception $e) {
            $this->logger->error('Error loading spreadsheet: ' . $e->getMessage());
            return;
        }

        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray(null, true, true, true);

        foreach ($rows as $row) {
            if ($row['A'] === 'Nom du groupe') {
                continue;
            }

            $group = new Group();
            $group->setGroupName($row['A']);
            $group->setOrigin($row['B']);
            $group->setCity($row['C']);
            $group->setStartYear(new \Datetime($row['D']));
            $group->setEndYear(new \Datetime($row['E']));
            $group->setFounders($row['F'] ?? '');
            $group->setMembersNumber($row['G']);
            $group->setGenre($row['H'] ?? '');
            $group->setPresentation($row['I']);

            $this->entityManager->persist($group);
        }

        $this->entityManager->flush();
    }
}
