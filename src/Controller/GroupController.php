<?php

namespace App\Controller;

use App\Entity\Group;
use App\Repository\GroupRepository;
use App\Service\ExcelImporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


#[Route('/groups')]
class GroupController extends AbstractController
{
    #[Route('/', name: 'groups_show', methods: ['GET'])]
    public function index(Request $request, GroupRepository $groupRepository): Response
    {
        // use paramConverter and queryValidation, voter if needed
        $itemPerPage = 100;
        $groups = $groupRepository->search([], $request->get('page', 1) - 1, $itemPerPage);

        return $this->json($groups);
    }

    #[Route('/{id}',  requirements: ['id' => '\d+'], name: 'group_show', methods: ['GET'])]
    public function show(Group $group): Response
    {
        return $this->json($group);
    }

    #[Route(name: 'group_create', methods: ['POST'])]
    public function create(Group $group, EntityManagerInterface $entityManager): Response
    {
        $entityManager->persist($group);
        $entityManager->flush();

        return $this->json($group);
    }

    #[Route('/{id}',  requirements: ['id' => '\d+'], name: 'group_update', methods: ['PUT'])]
    public function update(Group $group, EntityManagerInterface $entityManager): Response
    {
        $entityManager->flush();

        return $this->json($group);
    }

    #[Route('/{id}',  requirements: ['id' => '\d+'],  name: 'group_delete', methods: ['DELETE'])]
    public function delete(Group $group, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($group);
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/import', name: 'group_import', methods: ['POST'])]
    public function import(Request $request, ExcelImporter $excelImporter): JsonResponse
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile instanceof UploadedFile) {
            throw new BadRequestHttpException('Invalid file.');
        }

        try {
            $path = $this->getParameter('uploads_directory') . '/' . uniqid() . '.' . $uploadedFile->getClientOriginalExtension();
            $uploadedFile->move($this->getParameter('uploads_directory'), $path);

            $groups = $excelImporter->import($path);

            return $this->json($groups, Response::HTTP_CREATED);
        } catch (FileException $e) {
            throw new BadRequestHttpException('Error uploading file.');
        } finally {
            if (isset($path) && file_exists($path)) {
                unlink($path);
            }
        }
    }
}
