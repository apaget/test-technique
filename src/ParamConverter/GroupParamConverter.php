<?php


namespace App\ParamConverter;

use App\Entity\Group;
use App\Repository\GroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class GroupParamConverter implements ParamConverterInterface
{
    public function __construct(private EntityManagerInterface $entityManager, private GroupRepository $groupRepository)
    {
        $this->entityManager = $entityManager;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === Group::class;
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $data = json_decode($request->getContent(), true);
        $id = $request->get('id');

        if ($id) {
            $group = $this->groupRepository->find($id);
        } else {
            $group = new Group();
        }

        if ($data) {
            $group->setGroupName($data['groupName'] ?? null);
            $group->setOrigin($data['origin'] ?? null);
            $group->setCity($data['city'] ?? null);
            $group->setStartYear(new \DateTime($data['startYear'] ?? 'now'));
            $group->setEndYear(isset($data['endYear']) ? new \DateTime($data['endYear']) : null);
            $group->setFounders($data['founders'] ?? null);
            $group->setMembersNumber($data['membersNumber'] ?? null);
            $group->setGenre($data['genre'] ?? null);
            $group->setPresentation($data['presentation'] ?? null);
        }

        $request->attributes->set($configuration->getName(), $group);
        return true;
    }
}
