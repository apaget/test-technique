<?php

namespace App\Serializer\Normalizer;

use App\Entity\Group;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class GroupNormalizer implements NormalizerInterface
{
    public function __construct(
        private ObjectNormalizer $normalizer,
    ) {
    }

    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof Group;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($object, $format, $context);

        // Ajoutez des propriétés supplémentaires à l'entité Group ici si nécessaire
        return $data;
    }
}
