export class Group {
  id: number | null;
  groupName: string;
  origin: string | null;
  city: string | null;
  startYear: number | null;
  endYear: number | null;
  founders: string[] | null;
  membersNumber: number | null;
  genre: string | null;
  presentation: string | null;

  constructor(data: Group = {} as Group) {
    this.id = data.id || null;
    this.groupName = data.groupName || '';
    this.origin = data.origin || null;
    this.city = data.city || null;
    this.startYear = data.startYear || null;
    this.endYear = data.endYear || null;
    this.founders = data.founders || null;
    this.membersNumber = data.membersNumber || null;
    this.genre = data.genre || null;
    this.presentation = data.presentation || null;
  }
}
