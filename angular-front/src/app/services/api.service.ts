import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpRequest, HttpEventType } from '@angular/common/http';
import { Observable, throwError, map } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl: string = 'https://localhost/';

  constructor(protected http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(() =>
      'Something bad happened; please try again later.');
  }

  get<T>(url: string): Observable<T> {
    return this.http.get<T>(this.baseUrl + url).pipe(
      catchError(this.handleError)
    );
  }

  post<T>(url: string, data: any): Observable<T> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<T>(this.baseUrl + url, data, { headers: headers }).pipe(
      catchError(this.handleError)
    );
  }

  put<T>(url: string, data: any): Observable<T> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<T>(this.baseUrl + url, data, { headers: headers }).pipe(
      catchError(this.handleError)
    );
  }

  delete<T>(url: string): Observable<T> {
    return this.http.delete<T>(this.baseUrl + url).pipe(
      catchError(this.handleError)
    );
  }

  public uploadFile(file: File, endpoint: string, headers: HttpHeaders): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);

    const uploadReq = new HttpRequest('POST', this.baseUrl + endpoint, formData, {
      headers: headers,
      reportProgress: true
    });

    return this.http.request(uploadReq).pipe(
      map((event) => {
        if (event.type === HttpEventType.Response) {
          return event.body;
        } else if (event.type === HttpEventType.UploadProgress) {
          if (event.total) {
            const percentDone = Math.round((100 * event.loaded) / event.total);
            console.log(`File is ${percentDone}% uploaded.`);
          }
        }
      })
    );
  }
}
