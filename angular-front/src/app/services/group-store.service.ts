import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { catchError, finalize, map, tap, filter, switchMap } from 'rxjs/operators';
import { Group } from '../models/group.model';
import { GroupService } from './group.service';
import { ApiService } from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class GroupStore {
  private groups: BehaviorSubject<Group[]> = new BehaviorSubject<Group[]>([]);
  public readonly groups$: Observable<Group[]> = this.groups.asObservable();
  public fullyLoad: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);;

  constructor(private groupService: GroupService, private apiService: ApiService) { }

  public async getGroups(): Promise<Group[]> {
    this.fullyLoad.next(false)
    let groups = await this.groupService.getGroups().toPromise() as Group[]
    this.fullyLoad.next(true)
    this.groups.next(groups)
    return groups;
  }

  public addGroup(group: Group): void {
    this.groupService.addGroup(group)
      .pipe(
        catchError(error => {
          console.error(error);
          return throwError('Error adding group');
        }),
        finalize(() => console.log('Request complete'))
      )
      .subscribe(addedGroup => {
        this.groups.next([...this.groups.getValue(), addedGroup]);
      });
  }

  public updateGroup(group: Group): void {
    this.groupService.updateGroup(group)
      .pipe(
        catchError(error => {
          console.error(error);
          return throwError('Error updating group');
        }),
        finalize(() => console.log('Request complete'))
      )
      .subscribe(updatedGroup => {
        const index = this.groups.getValue().findIndex(g => g.id === updatedGroup.id);
        if (index > -1) {
          const groups = [...this.groups.getValue()];
          groups[index] = updatedGroup;
          this.groups.next(groups);
        }
      });
  }

  public deleteGroup(id: number): void {
    this.groupService.deleteGroup(id)
      .pipe(
        catchError(error => {
          console.error(error);
          return throwError('Error deleting group');
        }),
        finalize(() => console.log('Request complete'))
      )
      .subscribe(() => {
        const index = this.groups.getValue().findIndex(g => g.id === id);
        if (index > -1) {
          const groups = [...this.groups.getValue()];
          groups.splice(index, 1);
          this.groups.next(groups);
        }
      });
  }

  public importGroupsFromExcelFile(file: File): Observable<any> {
    return this.apiService.uploadFile(file, `groups/import`, new HttpHeaders())
  }
  public getGroup(id: string): Observable<Group> {
    const existingGroup = this.groups.getValue().find(group => group.id === parseInt(id));
    if (existingGroup) {
      return of(existingGroup);
    } else {
      return this.groupService.getGroup(id).pipe(
        catchError(error => {
          console.error(error);
          return throwError('Error fetching group');
        }),
        tap(fetchedGroup => {
          this.groups.next([fetchedGroup, ...this.groups.getValue()]);
        })
      );
    }
  }
}
