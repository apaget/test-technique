import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { map, Observable } from 'rxjs';
import { Group } from '../models/group.model';
import { HttpRequest, HttpEventType, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GroupService extends ApiService {
  private endpoint: string = 'groups';


  getGroups(): Observable<Group[]> {
    return super.get<Group[]>(this.endpoint);
  }

  getGroup(id: string): Observable<Group> {
    return super.get<Group>(`${this.endpoint}/${id}`);
  }

  addGroup(group: Group): Observable<Group> {
    return super.post<Group>(this.endpoint, group);
  }

  updateGroup(group: Group): Observable<Group> {
    return super.put<Group>(`${this.endpoint}/${group.id}`, group);
  }

  deleteGroup(id: number): Observable<void> {
    return super.delete<void>(`${this.endpoint}/${id}`);
  }

}
