import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { GroupComponent } from './group-page/group/group.component';
import { GroupService } from '../services/group.service';
import { GroupStore } from '../services/group-store.service';
import { ApiService } from '../services/api.service';
import { FormsModule } from '@angular/forms';
import { GroupPageComponent } from './group-page/group-page.component';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { GroupEditComponent } from './group-page/group-edit/group-edit.component';
import { GroupFormComponent } from './group-page/group-form/group-form.component';



const routes: Routes = [
  { path: '', component: GroupPageComponent },
  { path: ':id', component: GroupEditComponent }
];

@NgModule({
  declarations: [
    GroupPageComponent,
    GroupComponent,
    GroupEditComponent,
    GroupFormComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    GroupService,
    GroupStore,
    ApiService
  ]
})
export class GroupModule { }
