import { GroupStore } from 'src/app/services/group-store.service';
import { catchError, tap } from 'rxjs/operators';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, finalize, Observable } from 'rxjs';
import { GroupService } from 'src/app/services/group.service';
import { Location } from '@angular/common';
import { Group } from 'src/app/models/group.model';

@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.scss']
})
export class GroupEditComponent {
  group: Group = new Group();
  isLoading = false;
  saveObservable: Observable<Group>;

  constructor(
    private route: ActivatedRoute,
    private groupService: GroupService,
    private groupStore: GroupStore,
    private location: Location
  ) {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.isLoading = true;
      this.saveObservable = this.groupStore.getGroup(id)
        .pipe(
          tap(group => this.group = group),
          finalize(() => this.isLoading = false)
        );
    } else {
      this.saveObservable = this.groupService.addGroup(this.group);
    }
  }

  ngOnInit() {
    this.saveObservable.subscribe()
  }

  onSave(group: Group): void {
    this.isLoading = true;
    this.saveObservable = group.id ? this.groupService.updateGroup(group) : this.groupService.addGroup(group);
    this.saveObservable
      .pipe(
        tap(savedGroup => {
          this.group = savedGroup;
        }),
        finalize(() => this.isLoading = false),
        catchError(error => {
          console.error(error);
          return EMPTY;
        })
      )
      .subscribe(() => this.location.back());
  }

  onCancel(): void {
    this.location.back()
  }
}
