import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Group } from 'src/app/models/group.model';

@Component({
  selector: 'app-group-form',
  templateUrl: './group-form.component.html',
  styleUrls: ['./group-form.component.scss']
})
export class GroupFormComponent {
  @Input() group: Group = new Group();
  @Output() save = new EventEmitter<Group>();
  @Output() cancel = new EventEmitter<void>();

  onSave(): void {
    this.save.emit(this.group);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  getKeys(): string[] {
    return Object.keys(this.group);
  }
}
