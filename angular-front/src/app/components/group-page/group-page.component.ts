import { last } from 'rxjs/operators';
import { Group } from './../../models/group.model';
import { Component, OnInit } from '@angular/core';
import { GroupStore } from 'src/app/services/group-store.service';
import { GroupService } from 'src/app/services/group.service';
import { ElementRef, ViewChild } from '@angular/core';


@Component({
  selector: 'app-group-page',
  templateUrl: './group-page.component.html',
  styleUrls: ['./group-page.component.scss']
})
export class GroupPageComponent implements OnInit {
  groups: Group[] = [];
  fileToUpload: File | null = null;
  @ViewChild('hiddenPicker') hiddenPicker: ElementRef = {} as ElementRef;

  isUploading: boolean = false;

  constructor(private groupStore: GroupStore) { }

  async ngOnInit() {
    try {
      this.groups = await this.groupStore.getGroups()
    } catch (e) {
      console.error('Unable to fetch data, accepter le certificat en allant sur http://localhost')
    }
  }

  get isLoading() {
    return this.groupStore.fullyLoad
  }

  openFileExplorer() {
    this.hiddenPicker.nativeElement.click()
  }

  handleFileInput(event: any) {
    this.fileToUpload = event.currentTarget.files.item(0);
    this.uploadFile()
  }

  uploadFile() {
    this.isUploading = true
    if (this.fileToUpload) {
      this.groupStore.importGroupsFromExcelFile(this.fileToUpload).pipe(last()).subscribe(
        async () => {
          console.log('File uploaded successfully.');
          this.groups = await this.groupStore.getGroups();
        },
        error => {
          console.error(error);
        },
        () => this.isUploading = false
      );
    }
  }
}
