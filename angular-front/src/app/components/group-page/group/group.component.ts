import { Component, Input } from '@angular/core';
import { Group } from 'src/app/models/group.model';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent {
  @Input() group: Group = new Group();
  @Input() showEdit: boolean = false;

  getYearRange(): string {
    if (this.group.startYear && this.group.endYear) {
      const startYear = new Intl.DateTimeFormat('fr-FR', { year: 'numeric' }).format(new Date(this.group.startYear));
      const endYear = new Intl.DateTimeFormat('fr-FR', { year: 'numeric' }).format(new Date(this.group.endYear));
      return startYear + ' - ' + endYear;
    } else if (this.group.startYear) {
      const startYear = new Intl.DateTimeFormat('fr-FR', { year: 'numeric' }).format(new Date(this.group.startYear));
      return startYear + ' - présent';
    } else if (this.group.endYear) {
      const endYear = new Intl.DateTimeFormat('fr-FR', { year: 'numeric' }).format(new Date(this.group.endYear));
      return 'jusqu\'en ' + endYear;
    } else {
      return '';
    }
  }

}
