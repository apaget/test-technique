import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupEditComponent } from './components/group-page/group-edit/group-edit.component';
import { GroupPageComponent } from './components/group-page/group-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/groups', pathMatch: 'full' },
  { path: 'groups', component: GroupPageComponent },
  { path: 'groups/:id/edit', component: GroupEditComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
