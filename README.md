# Test Technique

## Getting Started

1. Run `docker compose up`
2. Ouvrir `https://localhost` dans votre navigateur et [accepter les auto-generated TLS certificate](https://stackoverflow.com/a/15076602/1352334)
   ou cliquez sur parametre avance et continuez vers se site
3. Ouvrir `https://localhost:4242`

## Project

### Objectif

Réalisation d’un système d’import du fichier Excel en pièce jointe, avec une interface de gestion des données.

### Contexte

En tant qu’utilisateur, je souhaite pouvoir importer le fichier Excel dans une base de données afin de pouvoir consulter, modifier ou supprimer les informations depuis une interface graphique.

### Stack technique

La partie backoffice doit être réalisée avec Symfony.
La partie frontend doit être réalisée avec Angular.
Durée du test
Nous ne souhaitons pas que ce test excède les 7 heures (avec mise en place de la stack).

## Attendus

Une archive contenant : le code source, ainsi que le dump de la base de données. Le code doit être accessible sur un repository en ligne (GitHub par exemple).
